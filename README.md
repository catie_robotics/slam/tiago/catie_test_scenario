# catie_test_scenario

Simple integration tests

# Requirements and installation
Add this to your ~/.bashrc:
```
alias sb="source ~/.bashrc"
```
And source it:
```
source ~/.bashrc
```

From now on, you will execute sb pretty often. The writter of this README is not very sure when it is really needed, so you'll have to do it all the time.

Follow this tutorial to install PAL's simulation environment:

http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation



Inside tiago_public_ws/src, clone the 2 repos:
```
git clone git@gitlab.com:catie_robotics/slam/tiago/simple_navigation_goals.git
```
```
git clone git@gitlab.com:catie_robotics/slam/tiago/catie_particle_handler.git
```

In catie_particle_handler, set the branch to train_craziness (waiting for merge on master):
```
git fetch
git checkout train_craziness
```

Add this to the end of your ~/.bashrc:
```
source ~/tiago_public_ws/devel/setup.bash
```

And:
```
sb
catkin build
```

Inside tiago_public_ws/src:
```
git clone git@gitlab.com:catie_robotics/slam/tiago/catie_test_scenario.git
sb
catkin build
sb
```


# Usage
Start TIAGo's simulation. This assumes that you already created a map, if you did you can skip the next link. If you never did, follow this tutorial:
http://wiki.ros.org/Robots/TIAGo/Tutorials/Navigation/Mapping

Then:
```
roslaunch tiago_2dnav_gazebo tiago_navigation_public.launch
```

Start this node (use tabs to auto-complete, if it doesn't work then something is wrong, cf the Troubleshooting section):
```
rosrun catie_test_scenario catie_test_scenario.py
```

Enjoy.

(jk life is pain)

# Troubleshooting
Did you sb?


